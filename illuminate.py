import time
import board
import neopixel
from random import randrange

# On a Raspberry pi, use this instead, not all pins are supported
pixel_pin = board.D18

# The number of NeoPixels
num_pixels = 300

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)


def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos * 3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos * 3)
        g = 0
        b = int(pos * 3)
    else:
        pos -= 170
        r = 0
        g = int(pos * 3)
        b = int(255 - pos * 3)
    return (r, g, b) if ORDER in (neopixel.RGB, neopixel.GRB) else (r, g, b, 0)


def rainbow_cycle(wait):
    for j in range(255):
        for i in range(num_pixels):
            pixel_index = (i * 256 // num_pixels) + j
            pixels[i] = wheel(pixel_index & 255)
        pixels.show()
        time.sleep(wait)

def race(laps,length):
    lap=0
    i=0
    direction=1
    colour = wheel(randrange(255))
    while lap < laps:
        if (i == num_pixels-1 and direction == 1) or (i == 1 and direction == -1):
            colour = wheel(randrange(255))
            direction = direction * -1
            lap+=1
        pixels[i] = colour
        if i > length-1 and direction == 1:
            new_colour = tuple(int(channel/3) for channel in colour)
            pixels[i-length] = new_colour
        elif i < num_pixels-length and direction == -1:
            new_colour = tuple(int(channel/3) for channel in colour)
            pixels[i+length] = new_colour
        i+=direction
        pixels.show()
        time.sleep(0.08)

def race2(laps,length):
    fade_length=15
    lap=0
    i=0
    direction=1
    end_reached = False
    colour_fade=[]
    base_colour = wheel(randrange(255))
    # create the colours for the fade in/out
    for j in range(fade_length):
        colour_fade.append(tuple(int(channel*(1/(j+1))) for channel in base_colour))
    while lap < laps:
        # i is the position of the leading edge of the wave train

        # reverse the direction at either end and change the colour
        if (i == num_pixels-1 and direction == 1) or (i == 1 and direction == -1):
            base_colour = wheel(randrange(255))

            # create the colours for the fade in/out
            colour_fade=[]
            for j in range(fade_length):
                colour_fade.append(tuple(int(channel*(1/(j+1))) for channel in base_colour))

            end_reached = True
            direction = direction * -1
            lap+=1

        # set the wave front colour
        pixels[i] = base_colour

        # set the colour for the fade out pixels while the wavefront is ascending
        if direction == 1:
            j = 1
            while j<=fade_length and i-j >= 0:
                pixels[i-j] = colour_fade[j-1]
                j+=1
            pixels[i-fade_length-1] = (0,0,0)

        # set the colour for the fade out pixels while the wavefront is descending
        elif direction == -1:
            j = 1
            while j<=fade_length and i+j <= num_pixels-1:
                pixels[i+j] = colour_fade[j-1]
                j+=1
            if i+fade_length+1 <= num_pixels-1:
                pixels[i+fade_length+1] = (0,0,0)

        # advance to the next pixel
        i+=direction
        pixels.show()
        time.sleep(0.04)

while True:
    pixels.fill((0,0,0))
    pixels.show()
    #race(4,4)
    race2(4,1)
    # Comment this line out if you have RGBW/GRBW NeoPixels
    ##pixels.fill((255, 0, 0))
    # Uncomment this line if you have RGBW/GRBW NeoPixels
    # pixels.fill((255, 0, 0, 0))
    ##pixels.show()
    ##time.sleep(1)

    # Comment this line out if you have RGBW/GRBW NeoPixels
    ##pixels.fill((0, 255, 0))
    # Uncomment this line if you have RGBW/GRBW NeoPixels
    # pixels.fill((0, 255, 0, 0))
    ##pixels.show()
    ##time.sleep(1)

    # Comment this line out if you have RGBW/GRBW NeoPixels
    ##pixels.fill((0, 0, 255))
    # Uncomment this line if you have RGBW/GRBW NeoPixels
    # pixels.fill((0, 0, 255, 0))
    ##pixels.show()
    ##time.sleep(1)

    #rainbow_cycle(0.001)  # rainbow cycle with 1ms delay per step
